import os
import sys
import unittest
#from models import db, Book
from create_db import db, Book, Author, Publisher

class DBTestCases(unittest.TestCase):
    '''
    Tests inserting a new book in books table
    '''
    def test_book_insert(self):
        s = Book(title = 'C++', image_url = 'test.jpg', author = 'butler', description = 'This is just a test.', publisher = 'test_publisher', publication_date = '1/2/13', isbn = '1234', book_id = '0')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(title = 'C++').one()
        self.assertEqual(str(r.title), 'C++')
        r = db.session.query(Book).filter_by(image_url = 'test.jpg').one()
        self.assertEqual(str(r.image_url), 'test.jpg')
        r = db.session.query(Book).filter_by(author = 'butler').one()
        self.assertEqual(str(r.author), 'butler')
        r = db.session.query(Book).filter_by(description = 'This is just a test.').one()
        self.assertEqual(str(r.description), 'This is just a test.')
        r = db.session.query(Book).filter_by(publisher = 'test_publisher').one()
        self.assertEqual(str(r.publisher), 'test_publisher')
        r = db.session.query(Book).filter_by(publication_date = '1/2/13').one()
        self.assertEqual(str(r.publication_date), '1/2/13')

        db.session.query(Book).filter_by(title = 'C++').delete()
        db.session.commit()
        
    '''
    Tests inserting a new author in authors table
    '''
    def test_author_insert(self):
        s = Author(name = 'Butler', image_url = 'test.jpg', books = 'Programming', description = 'This is just a test.', born = '9/9/97', education = 'UT', nationality = 'test', alma_mater = 'UT', wikipedia_url = 'wiki/test', author_id = '0')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Author).filter_by(name = 'Butler').one()
        self.assertEqual(str(r.name), 'Butler')
        r = db.session.query(Author).filter_by(image_url = 'test.jpg').one()
        self.assertEqual(str(r.image_url), 'test.jpg')
        r = db.session.query(Author).filter_by(books = 'Programming').one()
        self.assertEqual(str(r.books), 'Programming')
        r = db.session.query(Author).filter_by(description = 'This is just a test.').one()
        self.assertEqual(str(r.description), 'This is just a test.')
        r = db.session.query(Author).filter_by(born = '9/9/97').one()
        self.assertEqual(str(r.born), '9/9/97')
        r = db.session.query(Author).filter_by(education = 'UT').one()
        self.assertEqual(str(r.education), 'UT')
        r = db.session.query(Author).filter_by(nationality = 'test').one()
        self.assertEqual(str(r.nationality), 'test')
        r = db.session.query(Author).filter_by(alma_mater = 'UT').one()
        self.assertEqual(str(r.alma_mater), 'UT')
        r = db.session.query(Author).filter_by(wikipedia_url = 'wiki/test').one()
        self.assertEqual(str(r.wikipedia_url), 'wiki/test')

        db.session.query(Author).filter_by(name = 'Butler').delete()
        db.session.commit()
        
        
    '''
    Tests inserting a new publisher into publishers table
    '''
    def test_publisher_insert(self):
        s = Publisher(name = 'Butler', image_url = 'test.jpg', books = 'Programming', description = 'This is just a test.', owner = 'butler', website = 'UT', wikipedia_url = 'wiki/test', publisher_id = '0')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Publisher).filter_by(name = 'Butler').one()
        self.assertEqual(str(r.name), 'Butler')
        r = db.session.query(Publisher).filter_by(image_url = 'test.jpg').one()
        self.assertEqual(str(r.image_url), 'test.jpg')
        r = db.session.query(Publisher).filter_by(books = 'Programming').one()
        self.assertEqual(str(r.books), 'Programming')
        r = db.session.query(Publisher).filter_by(description = 'This is just a test.').one()
        self.assertEqual(str(r.description), 'This is just a test.')
        r = db.session.query(Publisher).filter_by(owner = 'butler').one()
        self.assertEqual(str(r.owner), 'butler')
        r = db.session.query(Publisher).filter_by(website = 'UT').one()
        self.assertEqual(str(r.website), 'UT')
        r = db.session.query(Publisher).filter_by(wikipedia_url = 'wiki/test').one()
        self.assertEqual(str(r.wikipedia_url), 'wiki/test')

        db.session.query(Publisher).filter_by(name = 'Butler').delete()
        db.session.commit()
    
    '''
    Tests if data is inserted correctly into each table from books.json
    '''
    def test_json_insert(self):
        
        # test book table
        r = db.session.query(Book).filter_by(book_id = 1).one()
        self.assertEqual(str(r.title), "Harry Potter and the Sorcerer's Stone")
        self.assertEqual(str(r.image_url), 'https://books.google.com/books/content/images/frontcover/wrOQLV6xB-wC?fife=w500')
        self.assertEqual(str(r.author), 'J. K. Rowling')
        self.assertEqual(str(r.description), "\"Turning the envelope over, his hand trembling, Harry saw a purple wax seal bearing a coat of arms; a lion, an eagle, a badger and a snake surrounding a large letter 'H'.\" Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry's eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!")
        self.assertEqual(str(r.publisher), 'Pottermore')
        self.assertEqual(str(r.publication_date), '2015-12-08')
        self.assertEqual(str(r.isbn), '9781781100486')
        
        # test author table
        s = db.session.query(Author).filter_by(author_id = 1).one()
        self.assertEqual(str(s.name), 'J. K. Rowling')
        self.assertEqual(str(s.image_url), 'http://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/J._K._Rowling_2010.jpg/220px-J._K._Rowling_2010.jpg')
        self.assertEqual(str(s.books), "Harry Potter and the Sorcerer's Stone, Harry Potter and the Chamber of Secrets, Harry Potter and the Prisoner of Azkaban, Harry Potter and the Goblet of Fire, Harry Potter and the Order of the Phoenix, Harry Potter and the Half-Blood Prince, Harry Potter and the Deathly Hallows")
        self.assertEqual(str(s.description), "Joanne \"Jo\" Rowling, OBE, FRSL, pen names J. K. Rowling and Robert Galbraith, is a British novelist, screenwriter and film producer best known as the author of the Harry Potter fantasy series. ")
        self.assertEqual(str(s.born), '1965-07-31')
        self.assertEqual(str(s.education), 'Bachelor of Arts')
        self.assertEqual(str(s.nationality), 'British')
        self.assertEqual(str(s.alma_mater), 'University of Exeter')
        self.assertEqual(str(s.wikipedia_url), 'https://en.wikipedia.org/wiki/J._K._Rowling')
        
        # test publisher table
        t = db.session.query(Publisher).filter_by(publisher_id = 1).one()
        self.assertEqual(str(t.name), 'Pottermore')
        self.assertEqual(str(t.image_url), 'http://upload.wikimedia.org/wikipedia/en/thumb/6/6f/Pottermore.png/225px-Pottermore.png')
        self.assertEqual(str(t.books), "Harry Potter and the Sorcerer's Stone")
        self.assertEqual(str(t.description), "Pottermore is the digital publishing, e-commerce, entertainment, and news company from J.K. Rowling and is the global digital publisher of Harry Potter and J.K. Rowling's Wizarding World.")
        self.assertEqual(str(t.owner), 'J. K. Rowling')
        self.assertEqual(str(t.website), 'http://www.pottermore.com\nshop.pottermore.com')
        self.assertEqual(str(t.wikipedia_url), 'https://en.wikipedia.org/wiki/Pottermore')
        
if __name__ == '__main__':
    unittest.main()
