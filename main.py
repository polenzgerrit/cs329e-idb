from flask import Flask, render_template, redirect, request, url_for
#from models import app, db, Book
from sqlalchemy import asc, desc
from create_db import app, db, Book, Author, Publisher, create_books, create_authors, create_publishers

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/books/')
def books():
    page = request.args.get('page',1, type=int)
    sort = request.args.get('sort',1, type=int)
    if sort == 1:
        book = db.session.query(Book).paginate(page=page,per_page=8)
    elif sort == 3:
        book = db.session.query(Book).order_by(desc('title')).paginate(page=page,per_page=8)
    elif sort == 2:
    	book = db.session.query(Book).order_by(asc('title')).paginate(page=page,per_page=8)
    return render_template('books.html', book = book, sort = sort)

@app.route('/abook/')
def abook():
    title = request.args.get('title','', type=str)
    book = db.session.query(Book).filter_by(title = title.strip())
    return render_template('abook.html', book = book)

@app.route('/authors/')
def authors():
    page = request.args.get('page',1, type=int)
    author = db.session.query(Author).paginate(page=page, per_page=6)
    return render_template('authors.html', author = author)

@app.route('/anauthor/')
def anauthor():
    name = request.args.get('name','', type=str)
    author = db.session.query(Author).filter_by(name = name.strip())
    return render_template('anauthor.html', author = author)

@app.route('/publishers/')
def publishers():
    page = request.args.get('page',1, type=int)
    publisher = db.session.query(Publisher).paginate(page=page, per_page=6)
    return render_template('publishers.html', publisher = publisher)

@app.route('/apublisher/')
def apublisher():
    name = request.args.get('name','', type=str)
    publisher = db.session.query(Publisher).filter_by(name = name.strip())
    return render_template('apublisher.html', publisher = publisher)



if __name__ == "__main__":
    app.run(debug=True)
